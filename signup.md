---
layout: page
title: "Presentation Signup"
weight: 4
---
<form class="pure-form" action="//formspree.io/signup@wmicc.org" method="POST">
    <fieldset>
        <div class="pure-control-group">
            <label for="name">Name(s)*</label><br>
            <input style="width:100%;" name="name" type="text" placeholder="You and your partner's names" required><br><br>
        </div>
        <div class="pure-control-group">
            <label for="title">Presentation Title*</label><br>
            <input style="width:100%;" name="title" type="text" placeholder="What culture are you presenting?" required><br><br>
        </div>
        <div class="pure-control-group">
            <label for="length">Length of presentation*</label><br>
            <input style="width:100%;" name="length" type="text" placeholder="15 minutes? 30 minutes? 5??" required><br><br>
        </div>
        <div class="pure-control-group">
            <label for="email">Email Address</label><br>
            <input style="width:100%;" name="email" type="email" placeholder="So we can respond to your request"><br><br>
        </div>
        <div class="pure-control-group">
            <label for="message">Any other information</label><br>
            <textarea style="width:100%; height:300px;" name="message" placeholder="Enter something here..."></textarea><br><br>
        </div>
        <input type="hidden" name="_next" value="/contact/thanks"/>

        <div class="pure-controls">
            <button type="submit" class="pure-button pure-button-primary">Send</button>
        </div>
    </fieldset>
</form>
