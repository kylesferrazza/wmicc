---
layout: default
---
<script>
function openEssay(evt, essayName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(essayName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

<style>
/* Style the tab */
div.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
div.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    height: 100%;
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.tabcontent iframe {
  width: 100%;
  height: 500px;
}

.tabcontent {
      -webkit-animation: fadeEffect 1s;
          animation: fadeEffect 1s; /* Fading effect takes 1 second */
}

@-webkit-keyframes fadeEffect {
      from {opacity: 0;}
          to {opacity: 1;}
}

@keyframes fadeEffect {
      from {opacity: 0;}
          to {opacity: 1;}
}
</style>

<h1>American Students</h1>
<div class="tab">
{% for i in (1..7) %}
  <button class="tablinks" onclick="openEssay(event, 'usa-{{i}}')">{{i}}</button>
{% endfor %}
</div>

{% for essay in site.data.usa %}
<div id="usa-{{essay.num}}" class="tabcontent">
  <h3>USA Essay {{essay.num}}</h3>
  <iframe src="https://docs.google.com/document/d/{{essay.doc}}/pub?embedded=true"></iframe>
</div>
{% endfor %}

<h1>Chinese Students</h1>
<div class="tab">
{% for i in (1..7) %}
  <button class="tablinks" onclick="openEssay(event, 'china-{{i}}')">{{i}}</button>
{% endfor %}
</div>

{% for essay in site.data.china %}
<div id="china-{{essay.num}}" class="tabcontent">
  <h3>China Essay {{essay.num}}</h3>
  <iframe src="https://docs.google.com/document/d/{{essay.doc}}/pub?embedded=true"></iframe>
</div>
{% endfor %}
