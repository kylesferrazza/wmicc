---
layout: page
title: Board Members
weight: 2
---
Our board members for the 2016-2017 school year are:

* President Kevin Wen
* Webmaster Kyle Sferrazza
* Vice Presidents Kavya Tangella, Ray Wei, and Jeffrey Chen
* Secretary Michael Selvaggio
* Treasurer Ryan Biks
* Photographer Jesse Wong
