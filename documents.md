---
layout: page
title: Documents
---
{% assign pages_list = site.pages | sort:"weight" %}
{% for node in pages_list %}
  {% if node.title != null %}
    {% if node.layout == "doc"%}{% unless node.title == "Presentation Schedule" %}
<a class="sidebar-nav-item{% if page.url == node.url %} active{% endif %}" href="{{ node.url }}">{{ node.title }}</a>
	{% endunless %}{% endif %}
  {% endif %}
{% endfor %}
