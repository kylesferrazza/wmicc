# WMHS ICC
Website for the International Cultures Club at WMHS.

## TODO
This todo is taken directly from emails sent by Kevin Wen himself.
- [x] Generated event pages
- [x] check _site for unneeded files in build
- [x] change color
- [ ] Update the Showcase with recent photos
- [ ] In that special folder for the videos please include a list of intructions on how to access a video and how to make a video full screen, not many people are computer savvy (like me)
- [x] Under the tab "Events" add another one called "Video Exchange Project" under the "Bake Sale" because we're actually going to do that project now
- [x] Please add the Video Consent Form to the website as an attachment.
- [x] Fix navigation in desktop mode for lower resolutions (maybe remove tagline)
