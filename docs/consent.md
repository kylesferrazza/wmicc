---
layout: doc
title: Video Consent Form
weight: 5.5
doc: 1tVld6UbNWlWkDdSiD2e4I9NdyHz--9F1geVbIYY_MCw
---
[Click here](https://docs.google.com/document/d/1tVld6UbNWlWkDdSiD2e4I9NdyHz--9F1geVbIYY_MCw/edit?usp=sharing) to access the form for printing.