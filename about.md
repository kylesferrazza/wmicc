---
layout: page
title: About
weight: 1
---
International Cultures Club is a club aimed at teaching Ward Melville students about different cultures through fun and interactive presentations.<br>
Each week, a different culture will be demonstrated and a food from that culture will possibly be served. Presentations may include background of the culture, a language lesson, crafts, cusine, and entertainment.<br>
Join us every Tuesday right after school in room 525 (new science wing)! All are welcome.<br>
[Find us on facebook][facebook].
And sign up for our [remind class][remind]!
[Contact us](/contact) with any questions, comments, concerns, or jokes!
<br>
<h4>&quot;The only way to have a tree is to plant a seed.&quot;</h4><h5>-Johnny Mo</h5>

[facebook]: https://www.facebook.com/groups/wminternationalcultures
[remind]: https://www.remind.com/join/wmicc