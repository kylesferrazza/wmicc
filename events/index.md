---
layout: page
title: "Events"
weight: 8
---
<p>Click any event title for signups.</p>
<hr>
<div class="posts">
  {% assign pages_list = site.pages | sort:"date" %}
  {% for post in pages_list %}
    {% if post.layout == "event" %}
    {% if post.strike %}<strike>{% endif %}
      <div class="post" style="margin-bottom:0px;">
        <h2 class="post-title">
          <a href="{{post.signup}}">{{ post.title }}</a>
        </h2>
        
        <span class="post-date">{{ post.date | date: '%B %d, %Y' }}{% if post.times %}, {{post.times}}{% endif %}</span>
        
        {{ post.content }}
        <hr>
      </div>
    {% if post.strike %}</strike>{% endif %}
    {% endif %}
  {% endfor %}
</div>