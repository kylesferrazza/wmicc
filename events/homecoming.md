---
layout: event
title: "Homecoming"
date: 2016-09-24
times: 10am-2pm or later
signup: https://goo.gl/forms/ErYHgnaqdpEZqDzw1
strike: true
---
Sign up to help us out at homecoming by running the international cultures club stand!
