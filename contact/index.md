---
layout: page
title: Contact Us
weight: 5
---
Need to get in contact? Fill out the form on this page.<br>

<form class="pure-form" action="//formspree.io/kyle.sferrazza@gmail.com" method="POST">
    <fieldset>
        <div class="pure-control-group">
            <label for="name">Name</label><br>
            <input name="name" type="text" placeholder="First &amp; Last"><br><br>

            <label for="email">Email Address</label><br>
            <input name="email" type="email" placeholder="So we can respond"><br><br>

            <label for="message">Questions, Comments, Suggestions</label><br>
            <textarea style="width:100%; height:150px;" name="message" placeholder="Tell us anything"></textarea><br><br>
        </div>

        <input type="hidden" name="_next" value="/contact/thanks"/>
        <input type="hidden" name="_subject" value="Contact Form"/>

        <div class="pure-controls">
            <button type="submit" class="pure-button pure-button-primary">Send</button>
        </div>
    </fieldset>
</form>
